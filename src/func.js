const { unloadFile } = require("mocha");

const getSum = (str1, str2) => {
    let strval1 = "", strval2 = "";
    if (str1.constructor.name == "Array") {
        return false;
    }
    else {
        strval1 = makeNumber(str1);
    }
    if (str2.constructor.name == "Array") {
        return false;
    }
    else {
        strval2 = makeNumber(str2);
    }
    if (strval1 === "" && strval2 !== "") {
        return strval2;
    }
    if (strval2 === "" && strval1 !== "") {
        return strval1;
    }
    if (strval2 === "" && strval1 === "") {
        return false;
    }
    if (strval2 !== "" && strval1 !== "") {
        let newstr = "";
        for (let i = 0; i < strval1.length; i++) {
            let num1 = Number(strval1[i]);
            let num2 = Number(strval2[i]);
            let sum = num1 + num2;
            newstr = newstr.concat(sum);
        }
        return newstr;
    }
};

function makeNumber(string) {
    let final = '';
    for (let i = 0; i < string.length; i++) {
        let num = Number(string[i]);
        if (!isNaN(num))
            final = final.concat(num);
    }
    return final;
}


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

    let countposts = 0, countcomments = 0;
    if (authorName.constructor.name == "Array" || authorName === "") {
        return 'Post:0,comments:0';
    }

    for (var i = 0; i < listOfPosts.length; i++) {

        var obj = listOfPosts[i];
        for (var key in obj) {
            var value = obj[key];

            if (typeof value !== "string") {
                for (var j = 0; j < value.length; j++) {

                    var objinner = value[j];
                    for (var keyinner in objinner) {
                        var valueinner = objinner[keyinner];
                        if (keyinner === "author") {
                            if (authorName === valueinner)
                                countcomments++;
                        }

                    }
                }
            }
            else {
                if (key === "author") {
                    if (authorName === value)
                        countposts++;
                }
            }
        }
    }
    return 'Post:' + countposts + ',comments:' + countcomments;
};

const tickets = (people) => {
    let sum = 0;
    for (let i = 0; i < people.length - 1; i++) {
        sum = sum + people[i];
    }
    if (sum === people[people.length - 1])
        return 'YES';
    else
        return 'NO';

};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
